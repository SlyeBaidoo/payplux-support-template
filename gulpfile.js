/*jshint esversion: 6 */

const gulp   = require('gulp');
const uglify = require("gulp-uglify-es").default;
const sass   = require("gulp-sass");
const pug    = require("gulp-pug");
const concat = require("gulp-concat");
const surge = require("gulp-surge");

gulp.task("js", function(cb) {
    return gulp
        .src(["src/js/**/*.js"])
        .pipe(concat('support.js'))
        .pipe(uglify())
        .pipe(gulp.dest("./dist/js/"));
});

gulp.task('css', function() {
    return gulp
        .src(['./src/css/**/*.scss'])
        .pipe(sass({ outputStyle: 'compressed' })
            .on('error', sass.logError))
        .pipe(concat('support.css'))
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('pug', function() {
    return gulp
        .src('./src/html/*.pug')
        .pipe(pug({
            pretty: false
        }))
        .pipe(gulp.dest('./dist'));
});

gulp.task('deploy', function () {
    return surge({
        project: './dist',         // Path to your static build directory
        domain: 'test-article.surge.sh'  // Your domain or Surge subdomain
    })
})

gulp.task('build', gulp.parallel(['css', 'js', 'pug']));

gulp.task('serve', function () {
    gulp.watch("./src/js/**/*.js", gulp.parallel(['js']));
    gulp.watch("./src/css/**/*.scss", gulp.parallel(["css"]));
    gulp.watch("./src/html/**/*.pug", gulp.parallel(["pug"]));
});
